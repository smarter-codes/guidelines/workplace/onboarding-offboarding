# Onboarding Offboarding

# Onboarding
   We are happy to see you Onboarded & to make a difference in the growth of **Smarter.Codes** as well as your career. We are a team of experts in different skills & we **respect each others experties** in smarter.codes, we do expect **polite conversation** when we talk with each other during the project discussions. We do expect everyone to **follow the guidelines** provided to them to deliver our clients requirement successfully. 
   
   If you get access to [This Link](https://gitlab.com/smarter-codes/guidelines/workplace) you can read guidelines related to performance appraisal, Communication, Leadership, about Workplace etc.. 


   - Talk to your **Recruitment HR** regarding the **Contact Person** for further Onboarding support related to below stuffs 
   
      1. Company Email Account access
      2. [MS Teams](https://teams.microsoft.com) - In its desktop & mobile application you will be able to communicate with the team members, The features of the tool is like chatting, Voice calling & Video calling. In teams once you login you will find several channels and groups which may be relevant to your work you will be getting assigned to.
      3. [Hubstaff.com](Hubstaff.com) - This tool will help you to track time you are utilizing on each project Tasks, it is linked with your weekly payment so do not forgot to use it whenever you work to get accurate funds processed in each week.
      4. [Airtable.com](Airtable.com) - In this tool you will be updating your weekly hours on each Monday, Your project manager will assign you tasks, you can ask for the project name & project manager name you were recruited for to get work assigned. Also this tool will give you access to different other sheets like for the asset management, people management etc.
      5. [Gitlab.com](Gitlab.com) or [Trello.com](Trello.com) - These tools will help you to share work requirements, and assign tasks to each other in the other team members.
      6. [Wiki.smarter.codes](https://wiki.smarter.codes/) - This link will give you basic idea about the best practices followed at smarter.codes & will help you to Understand the workflow and culture of our company, Spend some time in reading topics of this page which you find helpfull for you. 

After successfully completing Onboarding formalities you can start your great growth opportunities with your project manager and assigned buddy for your daily tasks. Initially for couple of weeks try to focus on your daily assigned tasks, as soon as you gets used to your daily tasks & if you got some free time from the assigned project, you can start exploring your skills and other projects. This will give you an opportunity to engage your weekly hours for 40 hours. As much as different type of skills you learn and explore you will have great long term exposure in the company.

**Best of Luck!**

# Offboarding guidelines

Despite of great **opportunities** and **benefits** there will always be an employee that decide to **move on** from **Smarter.Codes**, they may have thoroughly enjoyed their time at smarter.codes, but personal or professional reasons have encouraged them to move on. We do understand Making the decision to leave an organization is rarely easy. Most people will take weeks, if not months to consider their options before handing in their notice. When someone like you does make that decision, We know you're often hoping for as **smooth a transition** as possible.

- If you have taken the decision to leave the smarter.codes then please check with the below checkpoints & follow them accordingly to streamline your exit formalities.

     1. 30 Days Notice
     2. Knowledge transfer
     3. Return company assets
     4. Exit formalities
     5. Exit Survey or Interview

#  1. 30 Days Notice

**Talk** to your **project manager** as soon as you have taken the decision to **separate** from the **smarter.codes**. A 30-day resignation Notice is important because it provides **smarter.codes** plenty of time to find and train someone to replace you in your position. It also gives you enough time to finish the active projects you are working on. If you work with clients, a 30-day notice period gives you time to introduce your replacement to your clients for a smooth transition. 

If there is any posiblity of short Notice your project manager & HR team will confirm you, most of the time it is difficult to manage your job responsibilities within short time. Try to engage with your project manager and give him a day or two days time to take a call on your resignation  & Notice period Confirmation. This can all help you leave **smarter.codes** in good standing.

# 2. Knowledge transfer

Project Manager will assign someone with you during your Notice period to shadow you for your remaining tenure and to understand the project you are assigned with. You will be responsible to help the person to understand each and every aspect of the project to run smoothly in your absence. Also try to prepare some workflow documents of the methods you are following in the project which is going to help anyone with similar skills to execute your daily tasks easily. 

To prevent impact of your absence on the project deliverables and clients expectations you will have to help the assigned person thoroughly who is going to take your responsibilities of clients work. Remember that due to your great product experties client is considering you as a great guy, so transfering your project knowledge to the new person will also put you infront of client as a great person. 

#  3. Return Company Assets

In smarter.codes we usually send some company assets to the employees, it can be Laptop, Monitor, Keyboard, table, Monitor stand etc.. if any of such asset is delivered to you during your tenure you will have to return it to the company. Talk to HR they will suggest you the location where you will have to return the product. 

While returning the asset make sure its packed in the original packaging box which you have received with the product and choose the good courier company so that it can be safely delivered to the concerned person or company.

# 4. Exit formalities

Once you return the company assets and update HR about it your Exit formalities will get started, You will be getting your salary slips, relieving letter & Experience Certificate. This Exit formalities documentation procedure may take upto one or two weeks to get completed. Do not delete any document or file from your email account or drive, we may required it in future we will be keeping backup of your user account. As soon as your documentation is completed your accounts will be disabled permanently from accessing.

# 5. Exit Survey or Interview

We will try to arrange either Exit Survey or Exit Interview for you so that we can improve our employee engagement in future, try to be honest in the survey or Interview so that we can improve & it can be benefitial for other employees working in the company.
